from django.contrib import admin
from .models import Applications


class ApplicationsAdmin(admin.ModelAdmin):
    list_display = ('app_name','developer_name','published_date','app_price','charges_type')

admin.site.register(Applications, ApplicationsAdmin)
