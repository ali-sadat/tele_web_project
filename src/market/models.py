from django.db import models
from django.utils import timezone


class Applications(models.Model):
    app_name = models.CharField(max_length=200)
    developer_name = models.CharField(max_length=200)
    app_description = models.TextField
    published_date = models.DateField(default=timezone.now)
    app_price = models.DecimalField(decimal_places=2, max_digits=8, default=0)

    FREE = 'FR'
    HOURLY = 'HO'
    MONTHLY = 'MO'
    ANNUALLY = 'AN'

    CHARGES_TYPE = (
        (FREE, 'Free'),
        (HOURLY, 'Hourly'),
        (MONTHLY, 'Monthly'),
        (ANNUALLY, 'Annually'),
    )
    charges_type = models.CharField(
        max_length=2,
        choices=CHARGES_TYPE,
        default=FREE,
    )





